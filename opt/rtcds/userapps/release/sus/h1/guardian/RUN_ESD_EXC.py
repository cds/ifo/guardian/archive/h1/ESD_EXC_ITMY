##This part is going to run the Excitations for each of the diffrent QUADs
import os
import sys
import awg
import cdsutils
import time
import numpy
from guardian import GuardState, GuardStateDecorator
import gpstime

nominal = 'DOWN'
QUAD = _SYSTEM_[-4:]

drive_freq = {
    'ETMY':12,
    'ETMX':12,
    'ITMX':13,
    'ITMY':15,
}

# Common signal injection properties 
DURATION = 60
RAMP_TIME = 2

if QUAD == 'ETMX' or QUAD == 'ETMY':
    exc_amp_bias = 0.01		#  in volts to driver 
    exc_amp_DAL2L = 100
else:
    exc_amp_bias = 0.03		#  in volts to driver 
    exc_amp_DAL2L = 300
	    
# Dictionary to store injections and IFO inital parameters
inj = {}
ifo_params = {}

logfile = []


########################################################
##Functions 

def gpsnow():
    return int(gpstime.gpsnow())

def set_up_DAL2L():
    ezca.get_LIGOFilter('L3_DRIVEALIGN_L2L').only_on('OUTPUT', 'DECIMATION')
    time.sleep(1)
    ezca.get_LIGOFilter('L3_DRIVEALIGN_L2L').ramp_gain(1, ramp_time=1, wait=True)
    time.sleep(3)

def check_and_turn_off_L2A():
    if ezca['L3_DRIVEALIGN_L2P_GAIN'] != 0 or ezca['L3_DRIVEALIGN_L2Y_GAIN'] != 0:
        time.sleep(0.1)
        ezca['L3_DRIVEALIGN_L2P_GAIN']=0
        ezca['L3_DRIVEALIGN_L2Y_GAIN']=0
        return True
    else:
        return False

def check_and_turn_off_DAC_crossing_offsets():
    DAC_crossing_flag = False
    if ezca.get_LIGOFilter('L3_ESDOUTF_UL').is_on('OFFSET'):
        DAC_crossing_flag = True
        #turn off offsets that avoid DAC crossings
        for quadrant in ['UL', 'LL', 'UR', 'LR']:
            ezca.get_LIGOFilter('L3_ESDOUTF_%s'%(quadrant)).switch_off('OFFSET')
            time.sleep(ezca['L3_ESDOUTF_%s_TRAMP'%(quadrant)])
    return DAC_crossing_flag

def turn_on_DAC_crossing_offsets():
    #turn off offsets that avoid DAC crossings
    for quadrant in ['UL', 'LL', 'UR', 'LR']:
        ezca.get_LIGOFilter('L3_ESDOUTF_%s'%(quadrant)).switch_on('OFFSET')
        time.sleep(ezca['L3_ESDOUTF_%s_TRAMP'%(quadrant)])

def check_no_DARM_feedback():
    #FIXME: need to check for DAC crossing offsets
    #first check if DARM is fed back to this ESD, and if linearization is on
    if cdsutils.avg(5,'SUS-{}_L3_DRIVEALIGN_L_OUTMON'.format(QUAD)) != 0:
        log('DARM seems to be feeding back to this ESD, you need to move DARM control before running this measurement')
        return False
    else:   
        return True

def check_and_turn_on_bias():
    if ezca['L3_LOCK_INBIAS']!=9.3 or ezca['L3_LOCK_BIAS_OFFSET' ] != 0:
        log('Bias and/or offset not nominal for measurments. Setting to all be the same.')
        ezca.get_LIGOFilter('L3_LOCK_BIAS').ramp_gain(0, ramp_time=20, wait=True)
        if ezca['L3_LOCK_INBIAS']!=9.3:
            ezca.get_LIGOFilter('L3_LOCK_BIAS').switch_on('OFFSET', wait=False)
            ezca['L3_LOCK_BIAS_OFFSET' ] = 9.3
        if ezca['L3_LOCK_BIAS_OFFSET' ] != 0:
            ezca['L3_LOCK_BIAS_OFFSET' ] = 0.0
        ezca.get_LIGOFilter('L3_LOCK_BIAS').ramp_gain(1, ramp_time=20, wait=True)      
    if ezca['L3_LOCK_BIAS_GAIN']==0:
        log('bias was off (gain = 0), switching it on')
        ezca.get_LIGOFilter('L3_LOCK_BIAS' ).ramp_gain(1, ramp_time=20, wait=True)
        log('bias switching on')

def check_and_turn_off_linearization():
    # check if the ESD linearization is on
    if QUAD == 'ETMX' or QUAD == 'ETMY':
        if ezca['L3_ESDOUTF_UL_LIN_BYPASS_SW'] == 0:#if one quadrant is off they all are
            linearization_on_to_start = True
            log('ESD linearization is on, turning it off')
            slowly_ramp_off_bias()
            ##fliping the switches
            for quadrant in ['UL','LL','UR' ,'LR']:
                time.sleep(0.1)
                ezca['L3_ESDOUTF_%s_LIN_BYPASS_SW'%(QUAD,quadrant)] = 1
            log('linearization bypassed, ramping bias back on')
            slowly_ramp_on_bias()
            return True
    if QUAD == 'ITMX' or QUAD == 'ITMY':
        if ezca['L3_ESDOUTF_LIN_BYPASS_SW'] ==  0:
            linearization_on_to_start = True
            log('ITM linearization is on, turning it off')
            slowly_ramp_off_bias()
            time.sleep(1)
            ezca['L3_ESDOUTF_LIN_BYPASS_SW'] = 1
            log('linearization bypassed, ramping it back on')
            slowly_ramp_on_bias()
            return True

def slowly_ramp_off_bias():
    log('Ramping off bias on %s ESD'%QUAD)
    ezca.get_LIGOFilter('L3_LOCK_BIAS').ramp_gain(0, ramp_time=20, wait=True)


def slowly_ramp_on_bias():
    log('Ramping on bias on %s ESD'%QUAD)
    #turn the gain to zero before you start (this only works if the BIAS is 0 before you run this function)
    ezca.get_LIGOFilter('L3_LOCK_BIAS').ramp_gain(0, ramp_time=2, wait=True)
    ezca['L3_LOCK_BIAS_OFFSET'] = ifo_params['BIAS_value']
    time.sleep(0.1)
    ezca['L3_LOCK_INBIAS'] = ifo_params['INBIAS_value']
    if QUAD == 'ETMX' or QUAD == 'ETMY':
        ezca.get_LIGOFilter('L3_LOCK_BIAS').switch_on('OFFSET', wait=False)
    else:
        ezca.get_LIGOFilter('L3_LOCK_BIAS').switch_off('OFFSET', wait=False)
    ezca.get_LIGOFilter('L3_LOCK_BIAS').ramp_gain(ifo_params['BIAS_gain'], ramp_time=20, wait=True)
    #Are we leaving offset on/off correctly?


###########################################
# Making a wrapper to use for all the states that do the injections
def gen_PERFORM_INJ(exc_name, channel, ampl):
    # Non requestable state to do the injection, it takes parameters that will....
    class PERFORM_INJ(GuardState):
        request = False
        def main(self):
            inj[exc_name] = awg.Sine(channel, ampl, drive_freq[QUAD])
            inj[exc_name].start(ramptime=RAMP_TIME)
            self.gps_start = gpsnow()+RAMP_TIME
            log('Starting {}Hz Sine injection on {}'.format(drive_freq[QUAD], channel))
            self.timer['Injection duration'] = DURATION + RAMP_TIME
            self.clearflag = False
  
        def run(self):
            if self.timer['Injection duration'] and not self.clearflag:
                # Stop the injection because its full duration has elapsed
                log("Injection finished")
                # STOP THE INJECTION
                inj[exc_name].stop(ramptime=RAMP_TIME)
                inj[exc_name].clear()
                self.gps_stop = gpsnow()
                log('Stopping injection on {}'.format(channel))
                self.clearflag = True
            if self.clearflag:
                #need to save start and end times somewhere. 
                logfile.append([self.gps_start, self.gps_stop, exc_name])
                return True
            return False    
    return PERFORM_INJ
        
###########################################
###Starting Guardian

class INIT(GuardState):
    def main(self):
        return 'DOWN'

class DOWN(GuardState): 
    index = 1
    goto = True
    def main(self):
        for name, injection in inj.items():
            log("Stopping " + name)
            if injection.started:
                injection.stop(ramptime=RAMP_TIME)
        # Clear injections
        time.sleep(0.1)
        for name, injection in inj.items():
            log("Clearing " + name)
            injection.clear()
        #clear ifo_params. If lockloss, ISC_LOCK and svn revert can take care of params. 
        ifo_params = {}
        logfile.clear()
        return True

class SETUP(GuardState): 
    index = 3
    def main(self):
        log("Performing awg_cleanup to clear any old test points.")
        awg.awg_cleanup()
        #cleanup is needed after model restart, do everytime to ensure there is no stale test points.
        inj = {}
        if check_no_DARM_feedback():
            # record ifo inital parameters to be reset at the end of excitations 
            ifo_params['BIAS_gain'] = ezca['L3_LOCK_BIAS_GAIN']
            ifo_params['INBIAS_value'] = ezca['L3_LOCK_INBIAS']
            ifo_params['BIAS_value'] = ezca['L3_LOCK_BIAS_OFFSET']
            ifo_params['DA_L2L_gain'] = ezca['L3_DRIVEALIGN_L2L_GAIN']
            ifo_params['L2P_gain']=ezca['L3_DRIVEALIGN_L2P_GAIN']
            ifo_params['L2Y_gain']=ezca['L3_DRIVEALIGN_L2Y_GAIN']
            ifo_params['linearization_on_to_start'] = False
            ifo_params['L2L_tramp'] = ezca['L3_DRIVEALIGN_L2L_TRAMP']
            ifo_params['BIAS_tramp'] = ezca['L3_LOCK_BIAS_TRAMP']
            
            #set up suspension
            ifo_params['linearization_on_to_start'] = check_and_turn_off_linearization()
            #get ready to start the signal path excitations
            set_up_DAL2L()
            #if this sus has DAC crossing offsets of L2A coefficents turn them off, save info for restoring
            ifo_params['reset_L2A_flag'] = check_and_turn_off_L2A()
            ifo_params['DAC_crossing_flag'] = check_and_turn_off_DAC_crossing_offsets()
            check_and_turn_on_bias()
            
            log('starting in lock charge measurements')
            return True
            
        else:
            log('Not running the measurement since DARM is locked on this optic')
            return 'COMPLETE' 
        
BIAS_DRIVE_WITH_BIAS = gen_PERFORM_INJ('bias_drive_bias_on', 'H1:SUS-'+QUAD+'_L3_LOCK_BIAS_EXC', exc_amp_bias)
BIAS_DRIVE_WITH_BIAS.index = 10

L_DRIVE_WITH_BIAS = gen_PERFORM_INJ('L_drive_bias_on', 'H1:SUS-'+QUAD+'_L3_DRIVEALIGN_L2L_EXC', exc_amp_DAL2L)
L_DRIVE_WITH_BIAS.index = 11

class TURN_BIAS_OFF(GuardState): 
    index = 15
    def main(self):
        slowly_ramp_off_bias()
        #turn off inbias and turn gain back on to allow excitations through bias path
        ezca['L3_LOCK_INBIAS' ] = 0
        ezca['L3_LOCK_BIAS_OFFSET' ] = 0
        # previously bias gain, but this is sometimes 0 BIAS_gain
        ezca.get_LIGOFilter('L3_LOCK_BIAS' ).ramp_gain(1, ramp_time=2, wait=True)
        return True

BIAS_DRIVE_NO_BIAS = gen_PERFORM_INJ('bias_drive_bias_off', 'H1:SUS-'+QUAD+'_L3_LOCK_BIAS_EXC', exc_amp_bias)
BIAS_DRIVE_NO_BIAS.index = 16

L_DRIVE_NO_BIAS = gen_PERFORM_INJ('L_drive_bias_off', 'H1:SUS-'+QUAD+'_L3_DRIVEALIGN_L2L_EXC', exc_amp_DAL2L)
L_DRIVE_NO_BIAS.index = 17

class RESTORE_SETTINGS(GuardState):
    index = 25
    def main(self):
        log('Finished with all excitations')
        ### Save the logfile 	
        log_directory =  '/opt/rtcds/userapps/release/sus/common/scripts/quad/InLockChargeMeasurements/rec_LHO/'
        filename = log_directory + QUAD + '_'+ str(drive_freq[QUAD])+'_Hz_'+ str(gpsnow()) + '.txt'
        f = open(filename, 'w')
        for l in logfile:
            f.write("%d %d %s\n" % (l[0], l[1], l[2]))
        f.close()
        log("Saved GPS times in logfile: %s" %(filename))
	    
        log('Restoring things to the way they were before the measurement')
        if ifo_params['linearization_on_to_start']:
            log('turning linearization back on')
            if QUAD == 'ETMX' or QUAD == 'ETMY':
                for quadrant in ['UL','LL','UR' ,'LR']:
                    ezca['L3_ESDOUTF_%s_LIN_BYPASS_SW'%(quadrant)] = 0
            if QUAD == 'ITMX' or QUAD == 'ITMY':
                ezca['L3_ESDOUTF_LIN_BYPASS_SW' ] = 0
        # add if bias_on_to_start flag check here and at start? 
        slowly_ramp_on_bias()
        if ifo_params['reset_L2A_flag']:
            ezca['L3_DRIVEALIGN_L2P_GAIN' ] = ifo_params['L2P_gain']
            ezca['L3_DRIVEALIGN_L2Y_GAIN' ] = ifo_params['L2Y_gain']
        if ifo_params['DAC_crossing_flag']:
            turn_on_DAC_crossing_offsets() 
            #bias gain should already have been restored with an appropriate ramp
        ezca['L3_DRIVEALIGN_L2L_GAIN'] = ifo_params['DA_L2L_gain']
        time.sleep(2) # to stop glitch from changing filters alog75347
        ezca['L3_DRIVEALIGN_L2L_TRAMP'] = ifo_params['L2L_tramp'] 
        ezca['L3_LOCK_BIAS_TRAMP'] = ifo_params['BIAS_tramp']
        if cdsutils.avg(1,'SUS-{}_L3_DRIVEALIGN_L2L_INMON'.format(QUAD)) != 0 and ezca['L3_DRIVEALIGN_L2L_GAIN'] != 0:
            log('In a strange state: turning on L2L input will cause a lockloss unless gain is set to zero.')
            return False
        if QUAD == 'ETMX':
            ezca.get_LIGOFilter('L3_DRIVEALIGN_L2L').only_on('INPUT', 'DECIMATION', 'FM7', 'OUTPUT')
        if QUAD == 'ITMX':
            ezca.get_LIGOFilter('L3_DRIVEALIGN_L2L').only_on('INPUT', 'DECIMATION', 'FM4', 'FM5', 'OUTPUT')        
        if QUAD == 'ETMY' or QUAD == 'ITMY':
            ezca.get_LIGOFilter('L3_DRIVEALIGN_L2L').only_on('INPUT', 'DECIMATION', 'OUTPUT')
        log('all done')
        return True

class COMPLETE(GuardState):
    index = 30
    def run(self):
        return True

###########################################
##Edges
edges = [ 
	('DOWN', 'SETUP'),
	('SETUP', 'COMPLETE', 10),
	('SETUP' , 'BIAS_DRIVE_WITH_BIAS'),
	('BIAS_DRIVE_WITH_BIAS', 'L_DRIVE_WITH_BIAS'),	
	('L_DRIVE_WITH_BIAS','TURN_BIAS_OFF'),
	('TURN_BIAS_OFF', 'BIAS_DRIVE_NO_BIAS'),
	('BIAS_DRIVE_NO_BIAS', 'L_DRIVE_NO_BIAS'),	
	('L_DRIVE_NO_BIAS','RESTORE_SETTINGS'),
	('RESTORE_SETTINGS', 'COMPLETE')
	]



